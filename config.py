settings = dict(
	token = '',
#	author_id = 75668723473588224,
)

ownerID = 75668723473588224

api = dict(
	weather = '',
	omdb = '',
	youtube = '',
	openai = '',
)

urls = {
	'homepage': 'https://rikka.us',
	'Discord page': 'http://rikka.us/dc/',
#	'source': 'https://rikka.us/dc/source.php',
	'neofetch': 'https://rikka.us/neofetch.php',
	'publicManual': 'https://rikka.us/dc/manual/',
}

filetypes = {
	"image": ['image/apng', 'image/avif', 'image/gif', 'image/jpeg', 'image/png', 'image/svg+xml', 'image/webp', 'image/svg+xml'],
	"video": ['video/webm', 'video/ogg', 'video/mp4', 'video/quicktime', 'video/x-matroska', 'video/x-msvideo', 'video/3gpp', 'video/x-flv'],
	"audio": ['audio/webm', 'audio/ogg', 'audio/wave', 'audio/wav', 'audio/x-wav', 'audio/x-pn-wav', 'audio/mpeg', 'audio/mp4', 'audio/flac', 'audio/x-mpegurl'],
	"code": ['text/html'],
	"document": ['application/pdf'],
	"text": ['text/plain'],
	"application": ['application/x-msdos-program', 'application/octet-stream'],
	"archive": ['application/zip'],
}
