import discord
from discord.ext import commands, tasks
#from discord import app_commands
import config
from languages import en as lang
import sqlite3
import re
from termcolor import colored
from datetime import datetime
import time
import os
from os import listdir
from os.path import isfile, join
import traceback
import sys
import pprint

import logging
import logging.handlers

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
logging.getLogger('discord.http').setLevel(logging.DEBUG)

handler = logging.handlers.RotatingFileHandler(
	filename='discord.log',
	encoding='utf-8',
	maxBytes=32 * 1024 * 1024,  # 32 MiB
	backupCount=5,  # Rotate through 5 files
)
dt_fmt = '%Y-%m-%d %H:%M:%S'
formatter = logging.Formatter('[{asctime}] [{levelname:<8}] {name}: {message}', dt_fmt, style='{')
handler.setFormatter(formatter)
logger.addHandler(handler)

db = sqlite3.connect('bot.db')
intents = discord.Intents().all()
intents.members = True
client = commands.Bot(command_prefix='!', intents=intents)
client.remove_command('help')

commands.Cog.concealExistance = False

@client.event
async def on_ready():
	print(lang.lang['logged_in'].format(colored(str(client.user), 'green')) + str(discord.__version__))

	for filename in os.listdir('./modules'):
		if filename.endswith('.py'):
			try:
				await client.load_extension(f'modules.{filename[:-3]}')
				print(f'Loaded extension: {filename}')
			except Exception as e:
				print(f'Failed to load extension {filename}: {e}')
				print(traceback.print_exc())

	for filename in os.listdir('./essential_modules'):
		if filename.endswith('.py'):
			try:
				await client.load_extension(f'essential_modules.{filename[:-3]}')
				print(f'Loaded extension: {filename}')
			except Exception as e:
				print(f'Failed to load extension {filename}: {e}')
				print(traceback.print_exc())

# Disable single command from channel
@client.event
async def on_command(ctx):
	db = sqlite3.connect('bot.db')
	guild   = ctx.guild
	channel = ctx.channel
	command = ctx.command

	c = db.cursor()
	c.execute("SELECT 1 FROM disabled_commands_per_channel WHERE guild_id = ? AND channel_id = ? AND command = ?", [guild.id, int(channel.id), str(command)])

	if c.fetchone():
		print("Command disabled here")
		c.close()
		return

	c.close()
	pass

# Disable cog from channel
def is_cog_enabled(ctx):
	cogName = ctx.cog.__class__.__name__ if ctx.cog else None
	guild   = ctx.guild
	channel = ctx.channel

	db = sqlite3.connect('bot.db')
	c = db.cursor()
	c.execute("SELECT 1 FROM disabled_cogs_per_channel WHERE guild_id = ? AND channel_id = ? AND cog = ?", [guild.id, channel.id, cogName])
	result = c.fetchone()
	c.close()

	return not bool(result)

@client.event
async def on_message(message):
	if message.author.bot:
		return

	now = datetime.now()

	termString = time.strftime("%d/%m/%Y %H:%M:%S", time.localtime())

	if message.guild:
		termString += " (" +  colored(str(message.guild), 'blue') + ", "
		termString += colored(message.channel, 'yellow') + ") "

	termString += " " + colored(str(message.author), 'red') + ": "
	termString += message.content
	print(termString)

	if len(message.attachments) > 0:
		for attachment in message.attachments:
			print("File : " + attachment.url)

	try:
		linkfinder = re.findall('(?i)(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))?', message.content)
		if len(linkfinder[0]) > 200:
			await message.reply("Learn to shorten your links normie.", mention_author=False)
	except:
		pass

	try:
		await client.process_commands(message)
	except:
		errorFile = "errorlog/" + now.strftime("%d.%m.%Y") + "_onError.txt"
		f = open(errorFile, "a")
		f.write("Timestamp: " + now.strftime("%d.%m.%Y %H:%M:%S") + "\n")
		f.write(str(traceback.format_exc()) + "\n")
		f.close()
		print(colored("Error in on_message() detected by try except when executing process_commands():\n", 'red'))
		print(traceback.format_exc())

client.add_check(is_cog_enabled)
client.run(config.settings['token'])
