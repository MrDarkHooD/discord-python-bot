import discord
from discord.ext import commands


import config

class Ping(commands.Cog):
	'''
	Ping pong
	'''
	
	def __init__(self, client):
		self.client = client
		
	@commands.command()
	async def ping(self, ctx):
		latency = self.client.latency 
		await ctx.send(latency)
	
async def setup(client):
	await client.add_cog(Ping(client))
