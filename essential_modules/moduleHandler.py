import discord
from discord.ext import commands
from discord.ext.commands import check, Context
import config
import traceback
import sys

class ModuleHandler(commands.Cog):

	def __init__(self, client):
		self.client = client
		self.moduleDir = "modules"
		self.concealExistance = True
		
	@commands.command(pass_context = True, hidden = True)
	@commands.is_owner()
	async def load(self, ctx, moduleName):
		try:
			module = await self.client.load_extension(self.moduleDir + "." + moduleName)
		except Exception as e:
			print(f'Failed to load extension {filename}: {e}')
			print(traceback.print_exc())
			
		if not ctx.guild and ctx.author.id == self.client.bot.owner_id:
			await ctx.reply(str(module.info))
			await ctx.reply("Module " + moduleName + " loaded.")

	@commands.command(pass_context = True, hidden=True)
	@commands.is_owner()
	async def unload(self, ctx, moduleName):
		await self.client.remove_cog(self.moduleDir + "." + moduleName)
		await self.client.unload_extension(self.moduleDir + "." + moduleName)
		module = await self.client.get_cog(moduleName)
		
		if not ctx.guild and ctx.author.id == self.client.bot.owner_id:
			await ctx.reply(str(module.info))
			await ctx.reply("Module " + moduleName + " unloaded.")

	@commands.command(pass_context = True, hidden=True)
	@commands.is_owner()
	async def reload(self, ctx, moduleName):
		await self.client.remove_cog(self.moduleDir + "." + moduleName)
		await self.client.unload_extension(self.moduleDir + "." + moduleName)
		module = await self.client.load_extension(self.moduleDir + "." + moduleName)
			
		if not ctx.guild and ctx.author.id == self.client.bot.owner_id:
			await ctx.reply(str(module.info))
			await ctx.reply("Module " + moduleName + " reloaded.")

async def setup(client):
	await client.add_cog(ModuleHandler(client))
