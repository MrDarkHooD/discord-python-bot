import discord
from discord.ext import commands

import traceback
import sys

class ExceptionHandler(commands.Cog):
	
	def __init__(self, bot: commands.Bot) -> None:
		self.bot = bot
		self.concealExistance = True

	@commands.Cog.listener()
	async def on_command_error(self, ctx: commands.Context, error) -> None:
		
		message = ctx.message.content
		
		if isinstance(error, commands.CommandNotFound):
			if message == len(message) * message[0] and message[0] == prefix:
				print("!!!")
				# Someone probably sent "!!!!"
				return
			else:
				await ctx.send('Error: Command not found.')
				return
		
#		now = datetime.now()
		bot = self.bot
		command = ctx.command
		isHidden = command.hidden
		#permissions = ctx.channel.permissions_for(ctx.guild.me)
		#isChannelAdminOnly = permissions.administrator
		cog = command.cog
		prefix = bot.command_prefix
		
		
		# Bitches don't know about my commands, let's keep it that way
		if command.cog and command.cog.concealExistance:
			if ctx.guild is not None:
				print("Consealed command was triggered in guild")
				
			if ctx.author.id is not bot.owner_id:
				print("Consealed command was triggered by someone who shouldn't know about it.")
				
			if ctx.author.id == bot.owner_id and ctx.guild is None:
				await ctx.reply(error)
				await ctx.reply(traceback.format_exc())

			print(error)
			print(traceback.format_exc())
			return
		
		# command = ['call', 'class', 'delattr', 'dict', 'dir', 'doc', 'eq', 'format', 'ge', 'getattribute', 'gt', 'hash', 'init', 'init_subclass', 'le', 'lt', 'module', 'ne', 'new', 'original_kwargs', 'reduce', 'reduce_ex', 'repr', 'setattr', 'sizeof', 'slots', 'str', 'subclasshook', 'weakref', '_actual_conversion', '_after_invoke', '_before_invoke', '_buckets', '_callback', '_ensure_assignment_on_copy', '_get_converter', '_is_typing_optional', '_max_concurrency', '_parse_arguments', '_prepare_cooldowns', '_transform_greedy_pos', '_transform_greedy_var_pos', '_update_copy', 'add_check', 'after_invoke', 'aliases', 'before_invoke', 'brief', 'call_after_hooks', 'call_before_hooks', 'callback', 'can_run', 'checks', 'clean_params', 'cog', 'cog_name', 'cooldown_after_parsing', 'copy', 'description', 'dispatch_error', 'do_conversion', 'enabled', 'error', 'full_parent_name', 'get_cooldown_retry_after', 'help', 'hidden', 'ignore_extra', 'invoke', 'is_on_cooldown', 'module', 'name', 'params', 'parent', 'parents', 'prepare', 'qualified_name', 'reinvoke', 'remove_check', 'require_var_positional', 'reset_cooldown', 'rest_is_raw', 'root_parent', 'short_doc', 'signature', 'transform', 'update', 'usage']

		

		
			
		if isinstance(error, commands.MissingRequiredArgument):
			await ctx.send(f'Error: Not enough arguments. Use\n> {prefix}help {command}\nfor more info.')
			
			
		await ctx.reply(error)
		await ctx.reply(traceback.format_exc())
#		elif isinstance(error, commands.BadArgument):
#			await ctx.send('Error: Argument(s) are not valid. Use\n> "!help command"\nfor more info.')
#		elif isinstance(error, commands.NoPrivateMessage):
#			await ctx.send('Error: This command cannot be used in private chat.')
#		elif isinstance(error, commands.CommandInvokeError):
		
#			errorFile = "errorlog/" + now.strftime("%d.%m.%Y") + "_onError.txt"
#			errorMessage = "Timestamp: " + now.strftime("%d.%m.%Y %H:%M:%S") + "\n" + str(traceback.format_exc())
#			f = open(errorFile, "a")
#			f.write(errorMessage + "\n")
#			f.close()
	
#			print('\n\n')
#			print(colored("Command invoked error, detected by on_command_error():\n", 'red'))
#			print(errorMessage)
#			print(error)
#			print('\n\n')
		
#			await ctx.send("Error: Execution of this command invoked error:\n> " + str(error))
#		elif isinstance(error, commands.CommandOnCooldown):
#			await ctx.send('Error: This command is on cooldown.')
#		elif isinstance(error, commands.MaxConcurrencyReached):
#			await ctx.send('Error: Maxium concurrency reached.')
#		elif isinstance(error, commands.MissingPermissions):
#			await ctx.reply("")
#			await ctx.send("Error: Insufficient permissions.")
#		else:
#			await ctx.send("> " + str(error))
#			print(traceback.format_exc())

		pass # do your things here
		
async def setup(bot: commands.Bot) -> None:
	await bot.add_cog(ExceptionHandler(bot))
