import discord
from discord.ext import commands
from discord.errors import Forbidden

from config import urls

class Help(commands.Cog):
	
	def __init__(self, client):
		self.client = client

	@commands.command()
	async def help(self, ctx, *input):
		prefix = self.client.command_prefix

		if not input:

			embed = discord.Embed(
				title = "Commands and modules",
				color = 0xd70751,
				#url = config.urls['publicManual']
				type = "rich",
				description = "Use `" + prefix + "help <module>` to gain more information about that module\n"
			)
			embed.set_thumbnail(url="https://rikka.us/debian.png")	   

			# iterating trough cogs, gathering descriptions
			cogs_desc = ''
			for cog in self.client.cogs:
				
				if self.client.cogs[cog].concealExistance:
					continue
					
				if self.client.cogs[cog].__doc__:
					cogs_desc += f'`{cog}` {self.client.cogs[cog].__doc__.rstrip()}\n'
				else:
					cogs_desc += f'`{cog}`\n'
			
			embed.add_field(name='Modules', value=cogs_desc, inline=False)

			# integrating trough uncategorized commands
			commands_desc = ''
			for command in self.client.walk_commands():
				if not command.cog_name and not command.hidden and not command.cog.concealExistance:
					#commands_desc += f'{command.name} - {command.help}\n'
					commands_desc += f'{command.name}\n'

			# adding those commands to embed
			if commands_desc:
				embed.add_field(name='Not belonging to a module', value=commands_desc, inline=False)

			embed.add_field(name="", value="Manuals can be found from [here]("+ urls['publicManual'] +")")

		# block called when one cog-name is given
		# trying to find matching cog and it's commands
		elif len(input) == 1:
			input = str(input[0])
			embed = discord.Embed(title="Not found!",
								description=f"`{input}` is not valid command nor module.",
								color=discord.Color.orange())
			
			for command in self.client.commands:
				if input == str(command):
					commandName = input

					embed = discord.Embed(
						title=f'{prefix}{commandName} - Command',
						description=command.help)
					if command.usage:
						usageString = ""
						commandString = f"{prefix}{commandName} "
						for arg, inst in command.usage.items():
							usageString += f"{arg}: {inst}\n"
							commandString += f" {arg},"
							
						commandString = commandString.rstrip(",")
						embed.add_field(name=f"Arguments",
										value=f"{commandString}\n\n`{usageString}`",
										inline=False)
					break
			
			for cog in self.client.cogs:
				if str(cog) != input:
					continue
					
				if self.client.cogs[cog].concealExistance:
					# Panic flushdown
					await ctx.reply(embed=embed)
					pass


				# making title - getting description from doc-string below class
				embed = discord.Embed(title=f'{cog} - Commands',
									  description=self.client.cogs[cog].__doc__,
									  color=discord.Color.green())

				# getting commands from cog
				for command in self.client.get_cog(cog).get_commands():
					if not command.hidden:
						#print(command.permissions)
						#print(command.checks.has_permissions)
						try:
							can_use = await command.can_run(ctx)
							embed.add_field(name=f"`{prefix}{command.name}`",
											value=command.brief,
											inline=False)
						except:
							pass

		else:
			embed = discord.Embed(title="Too many arguments.",
								description="Ask data about one module at time.",
								color=discord.Color.orange())

		await ctx.reply(embed=embed)

async def setup(client):
#	client.help_command = customHelp()
	await client.add_cog(Help(client))
