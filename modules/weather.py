import discord
from discord.ext import commands
import config

import pycountry
import urllib
import json

class Weather(commands.Cog):
	def __init__(self, client):
		self.client = client

	@commands.command(brief="Returns weather data from given location", description="Returns weather data from given location", aliases=['w', 'sää'])
	async def weather(self, ctx, *, location):
		location = urllib.parse.quote_plus(location)
		weatherURL = "https://api.openweathermap.org/data/2.5/weather?lang=en&units=metric&q=" + location + "&appid=" + config.api['weather']

		async with ctx.channel.typing():
			try:
				with urllib.request.urlopen(weatherURL) as response:
					html = response.read()
					weatherData = json.loads(html)

					countryCode = weatherData['sys']['country']
					countryName = pycountry.countries.get(alpha_2=countryCode).official_name

					embed = discord.Embed(
						title = "Weather in " + weatherData['name'] + ", " + countryName,
						color=discord.Color.blue()
					)

					embed.set_thumbnail(url="http://openweathermap.org/img/wn/" + weatherData['weather'][0]['icon'] + "@2x.png")
					embed.add_field(name="Description", value=weatherData['weather'][0]['description'], inline=True)
					embed.add_field(name="Temperature", value=str(weatherData['main']['temp']) + "°C", inline=True)
					embed.add_field(name="Feels like", value=str(weatherData['main']['feels_like']) + "°C", inline=True)
					embed.add_field(name="Wind speed", value=str(weatherData['wind']['speed']) + "/ms", inline=True)
					embed.add_field(name="Pressure", value=str(weatherData['main']['pressure']) + "hPa", inline=True)
					embed.add_field(name="Humidity", value=str(weatherData['main']['humidity']) + "%", inline=True)
					embed.set_footer(text="Powered by https://openweathermap.org/")
					await ctx.reply(embed=embed)
			except urllib.error.HTTPError as e:
				await ctx.reply("Location not found")

async def setup(client):
	await client.add_cog(Weather(client))
