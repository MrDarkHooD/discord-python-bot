import discord
from discord.ext import commands, tasks
import sqlite3

from datetime import time, datetime
from random import randint

from languages.en import errors as errText, lottery as lotteryText

class Lottery(commands.Cog):
	def __init__(self, client):
		self.client = client
		self.db = sqlite3.connect('bot.db')
		self.lotteryDraw.start()

	def cog_unload(self):
		if self.lotteryDraw:
			self.lotteryDraw.cancel()
			
		pass
		
	@commands.command(aliases=['lotto'])
	@commands.guild_only()
	async def participate_lottery(self, ctx, n1: int, n2: int, n3: int, n4: int, bn: int):
		c = self.db.cursor()
		timeNow = datetime.now()
		weekNow = timeNow.strftime("%W")
		yearNow = timeNow.strftime("%Y")
		
		c.execute("SELECT lottery_channel FROM guild_settings WHERE guild_id = ?", [ctx.guild.id])
		lotteryChannelData = c.fetchone()
		if not lotteryChannelData:
			ctx.reply(errText["channelMissingCannotProceed"].format("lottery_channel"))
			c.close()
			return
		
		if len([*filter(lambda x: x > 9, [n1, n2, n3, n4, bn])]) > 0:
			await ctx.reply(lotteryText["errNumbersNotValid"])
			c.close()
			return
		
		if len([*filter(lambda x: x < 0, [n1, n2, n3, n4, bn])]) > 0:
		
			await ctx.reply(lotteryText["errNumbersNotValid"])
			c.close()
			return
		
		lotteryChannel = self.client.get_channel(lotteryChannelData[0])
		
		if ctx.channel.id != lotteryChannel.id:
			await ctx.reply(errText["commandRestrictedToChannel"].format(lotteryChannel.mention))
			c.close()
			return

		if (timeNow.weekday() == 5 and timeNow.hour >= 20 and timeNow.minute >= 55) or timeNow.weekday() == 6:
			await ctx.reply("Lottery participation closed, try again on monday.")
			c.close()
			return

		c.execute("SELECT 1 FROM lottery WHERE guild_id = ? AND user_id = ? AND year = ? AND week = ?",
				  [ctx.guild.id, ctx.author.id, yearNow, weekNow])
		if c.fetchone():
			await ctx.reply(lotteryText["errAlreadyParticipated"].format(weekNow))
			c.close()
			return
		
		c.execute("SELECT xp_amount FROM user_xp WHERE guild_id = ? AND user_id = ?",
				  [ctx.guild.id, ctx.author.id])
		xpAmount = c.fetchone()[0]
		
		if xpAmount < 200:
			await ctx.reply(lotteryText["errInvalidXPtoParticipate"].format(200))
			c.close()
			return

		c.execute("UPDATE user_xp SET xp_amount = xp_amount - 200 WHERE guild_id = ? AND user_id = ?",
				  [ctx.guild.id, ctx.author.id])
		
		c.execute("INSERT INTO lottery (user_id, n1, n2, n3, n4, bn, week, year, guild_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
				 [ctx.author.id, n1, n2, n3, n4, bn, weekNow, yearNow, ctx.guild.id])

		self.db.commit()
		c.close()
		await ctx.reply(lotteryText["participateSuccess"].format(n1, n2, n3, n4, bn))
	
	local_tz = datetime.now().astimezone().tzinfo
	lotteryTime = time(hour=21, minute=0, tzinfo=local_tz)
	
	@tasks.loop(time=lotteryTime)
	async def lotteryDraw(self):
		timeNow = datetime.now()
		weekNow = timeNow.strftime("%W")
		yearNow = timeNow.strftime("%Y")

		if timeNow.strftime("%a") != "Sat":
			return

		dn1, dn2, dn3, dn4, dnb = [randint(0, 9) for _ in range(5)]
		
		print(f"Lottery numbers of week {weekNow}: {dn1} {dn2} {dn3} {dn4} + {dnb}")
		
		c = self.db.cursor()
		
		for guild in self.client.guilds:
			praticipants = 0
			winners = 0
			jackpotters = 0
			c.execute("SELECT lottery_channel FROM guild_settings WHERE guild_id = ?", [guild.id])
			channelData = c.fetchone()
			if not channelData:
				continue

			lotteryChannel = self.client.get_channel(channelData[0])
			
			if not lotteryChannel:
				continue

			descStrin = f"Winning numbers are: {dn1} {dn2} {dn3} {dn4} + {dnb}\n" 
				
			c.execute("SELECT user_id, n1, n2, n3, n4, bn FROM lottery WHERE guild_id = ? AND week = ? AND year = ?",
					  [guild.id, weekNow, yearNow])
			for un in c.fetchall():
				praticipants += 1
				matches = sum(1 for dn, db_n in zip([dn1, dn2, dn3, dn4], un[1:]) if dn == db_n)

				if matches == 0:
					continue
					
				winners += 1

				user = self.client.get_guild(guild.id).get_member(un[0])
				xpGains = {0: 0, 1: 100, 2: 1000, 3: 10000, 4: 100000}
				xpGain = xpGains.get(matches, 0)
				
				formatNumbers = []
				for i, dn in enumerate([dn1, dn2, dn3, dn4, dnb], start=1):
					if dn == un[i]:
						formatNumbers.append(f"**{un[i]}**")
					else:
						formatNumbers.append(str(un[i]))
				
				formatNumbers.insert(4, "+")

				if dnb == un[5] and matches > 1:
					if matches == 4:
						xpGain = 1000000
						jackpotters += 1
					else:
						xpGain += 100

				userNumbersFormatted = " ".join(formatNumbers)
				descStrin += f"* {user.mention} {userNumbersFormatted} ({xpGain}xp)\n"

				c.execute("UPDATE user_xp SET xp_amount = xp_amount + ?, bonus_xp = bonus_xp + ? WHERE guild_id = ? AND user_id = ?",
						  [xpGain, xpGain, guild.id, user.id])
				self.db.commit()

			if winners == 0:
				descStrin += "No winners this week"
			
			embed = discord.Embed(
				title = f"Week {weekNow} lottery results",
				description=descStrin,
				color = 0xffd700
			)
			embed.set_thumbnail(url="https://rikka.us/lottery.jpg")
			embed.set_footer(text=f'This week there was {winners} winners out of {praticipants} praticipants. Jackpot hit {jackpotters} users.')

			await lotteryChannel.send(embed=embed)
		c.close()
		
	@commands.command()
	@commands.guild_only()
	async def lottery_rules(self, ctx):
		rules = """
* Participating cost's 200xp
* Numbers are 0-9
* 1 hit to any number, including bonus number, is worth 100xp
* 2 hits, not including bonus number, yelds 1000xp
* 3 hits, not including bonus number, yelds 10 000xp
* 1, 2 or 3 hits with bonus number yelds extra 100xp
* 4 hits, not including bonus number, yelds 100 000xp
* 5 correct numbers yeld 1 000 000xp
* Ticket buying time is from Mon 00:00 to Sat 20:55
* Draw is at 21:00 on every Saturday.
		"""
		await ctx.reply(rules)

	@lotteryDraw.before_loop
	async def beforeLottery(self):
		await self.client.wait_until_ready()

async def setup(client):
	await client.add_cog(Lottery(client))
