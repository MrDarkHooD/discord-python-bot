import discord
from discord.ext import commands

from collections import namedtuple
from pprint import pprint as pp
import math
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import io
import re
from sympy import preview
from PIL import Image, ImageChops
from string import Template
import requests
import aiohttp
import traceback

# Shunting and postfix are not made by me, probably found them from StackOverflow

OpInfo = namedtuple('OpInfo', 'prec assoc')
L, R = 'Left Right'.split()

ops = {
 '^': OpInfo(prec=4, assoc=R),
 '*': OpInfo(prec=3, assoc=L),
 '/': OpInfo(prec=3, assoc=L),
 '+': OpInfo(prec=2, assoc=L),
 '-': OpInfo(prec=2, assoc=L),
 '(': OpInfo(prec=9, assoc=L),
 ')': OpInfo(prec=0, assoc=L),
 }

NUM, LPAREN, RPAREN = 'NUMBER ( )'.split()

def shunting(inp):
	'Inputs an expression and returns list of (TOKENTYPE, tokenvalue)'

	tokens = inp.strip().split()
	tokenvals = []
	for token in tokens:
		if token in ops:
			tokenvals.append((token, ops[token]))
		#elif token in (LPAREN, RPAREN):
		#	tokenvals.append((token, token))
		else:
			tokenvals.append((NUM, token))

	outq, stack = [], []
	table = ['TOKEN,ACTION,RPN OUTPUT,OP STACK,NOTES'.split(',')]
	for token, val in tokenvals:
		note = action = ''
		if token is NUM:
			action = 'Add number to output'
			outq.append(val)
			table.append( (val, action, ' '.join(outq), ' '.join(s[0] for s in stack), note) )
		elif token in ops:
			t1, (p1, a1) = token, val
			v = t1
			note = 'Pop ops from stack to output' 
			while stack:
				t2, (p2, a2) = stack[-1]
				if (a1 == L and p1 <= p2) or (a1 == R and p1 < p2):
					if t1 != RPAREN:
						if t2 != LPAREN:
							stack.pop()
							action = '(Pop op)'
							outq.append(t2)
						else:
							break
					else:
						if t2 != LPAREN:
							stack.pop()
							action = '(Pop op)'
							outq.append(t2)
						else:
							stack.pop()
							action = '(Pop & discard "(")'
							table.append( (v, action, ' '.join(outq), ' '.join(s[0] for s in stack), note) )
							break
					table.append( (v, action, ' '.join(outq), ' '.join(s[0] for s in stack), note) )
					v = note = ''
				else:
					note = ''
					break
				note = ''
			note = '' 
			if t1 != RPAREN:
				stack.append((token, val))
				action = 'Push op token to stack'
			else:
				action = 'Discard ")"'
			table.append( (v, action, ' '.join(outq), ' '.join(s[0] for s in stack), note) )
	note = 'Drain stack to output'
	while stack:
		v = ''
		t2, (p2, a2) = stack[-1]
		action = '(Pop op)'
		stack.pop()
		outq.append(t2)
		table.append( (v, action, ' '.join(outq), ' '.join(s[0] for s in stack), note) )
		v = note = ''
	return table

def ReversePolishNotation(tokens):
	ops = {
	  "+": (lambda a, b: a + b),
	  "-": (lambda a, b: a - b),
	  "*": (lambda a, b: a * b),
	  "/": (lambda a, b: a / b),
	  "^": (lambda a, b: a ** b)
	}
	stack = []

	for token in tokens:
		if token in ops:
			arg2 = stack.pop()
			arg1 = stack.pop()
			result = ops[token](arg1, arg2)
			stack.append(result)
		else:
			stack.append(float(token))


	return stack.pop()

def latexToPNGbuffer(string):
	try:
		tex = "$"+string+"$"
		plt.rc('text', usetex=True)
		plt.rc('font', family='serif')
		fig, ax = plt.subplots(figsize=(1, 1))
		ax.text(0.5, 0.5, tex, fontsize=16, ha='center', va='center')
		plt.axis('off')
		buf = io.BytesIO()
		plt.savefig(buf, format='png', bbox_inches='tight', pad_inches=0.2)
		plt.close()

		im = Image.open(buf)
		output = io.BytesIO()
		im.save(output, format='PNG')
		output.seek(0)
		return output
		
		return output
	except Exception as e:
		print(e)
		print(traceback.print_exc())
	
class Math(commands.Cog):
	def __init__(self, client):
		self.client = client
		
	@commands.command(brief="Calculate postfix", description="Calculates reverse postfix notations")
	async def postfix_notation(self, ctx, * tokens):
		await ctx.send(ReversePolishNotation(tokens))

	@commands.command(brief="Calculate shunting yard", description="Calculates shunting yard")
	async def shunting_yard(self, ctx, *tokens):
		
		math = re.findall(r'\d+\.\d+|\d+|\+|\^|-|\*|/|\(|\)', "".join(tokens))
		
		await ctx.send(shunting(" ".join(math))[-1][2])

	@commands.command(brief="Calculator", description="Calculates math", aliases=['calc', 'c'])
	async def calculator(self, ctx, *math):
		math = re.findall(r'\d+\.\d+|\d+|\+|-|\*|/|\^|\(|\)', "".join(math))
		await ctx.send(ReversePolishNotation(shunting(" ".join(math))[-1][2].split()))

	@commands.command(brief="Calculate square root", description="Calculates square root of given number", aliases=['squareroot'])
	async def sqrt(self, ctx, number):
		await ctx.reply(math.sqrt(int(number)))
		
	@commands.command(hidden=True)
	async def oldlatex(self, ctx, *string):
		inputS = " ".join(string)
		imageBuffer = latexToPNGbuffer2(inputS)
		if not imageBuffer:
			await ctx.reply("Couldn't process request")
			return
		
		await ctx.reply(file=discord.File(imageBuffer, filename='latex.png'))
						
	@commands.command(help="https://matplotlib.org/stable/users/explain/text/mathtext.html")
	async def latex(self, ctx, *string):
		inputS = " ".join(string)
		imageBuffer = latexToPNGbuffer(inputS)
		if not imageBuffer:
			await ctx.reply("Couldn't process request")
			return
		
		await ctx.reply(file=discord.File(imageBuffer, filename='latex.png'))
	
						
async def setup(client):
	await client.add_cog(Math(client))
