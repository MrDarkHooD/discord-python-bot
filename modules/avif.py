import discord
from discord.ext import commands
import config
from languages import en as lang

import re
import requests
import time
import sqlite3
from PIL import Image
import pillow_avif
import aiohttp
import io

class Avif(commands.Cog):
	def __init__(self, client):
		self.client = client
		self.db = sqlite3.connect('bot.db')

		
	@commands.Cog.listener()
	async def on_message(self, message):
		if message.author == self.client.user:
			return

		if message.author.bot:
			return
		
		urlList = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', message.content)

		
		
		for attachment in message.attachments:
			urlList.append(attachment.url)
			
		if len(urlList) == 0:
			return
			
		for url in urlList:
			response = requests.get(url)
		
			mimetype = str(response.headers['content-type'])
			if mimetype == "image/avif":
				
				avif_image = Image.open(io.BytesIO(response.content))

				jpg_image = io.BytesIO()
				avif_image.save(jpg_image, 'jpeg')
				jpg_image.seek(0)

				await message.reply(file=discord.File(jpg_image, filename='image.jpg'))
				
		
async def setup(client):
	await client.add_cog(Avif(client))
