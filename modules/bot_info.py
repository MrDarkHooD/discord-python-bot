import discord
from discord.ext import commands
import config

import os
import platform
import psutil
import time
import distro

intervals = (
	('weeks', 604800),  # 60 * 60 * 24 * 7
	('days', 86400),	# 60 * 60 * 24
	('hours', 3600),	# 60 * 60
	('minutes', 60),
	('seconds', 1),
	)

# Not mine, don't remember where I copied this
def display_time(seconds, granularity=2):
	result = []

	for name, count in intervals:
		value = seconds // count
		if value:
			seconds -= value * count
			if value == 1:
				name = name.rstrip('s')
			result.append("{} {}".format(round(value), name))
	return ', '.join(result)

class botInfo(commands.Cog):
	
	def __init__(self, client):
		self.client = client

	@commands.command(brief=f' General info about this bot.')
	async def botGeneral(self, ctx):
		embed = discord.Embed(
			title="Bot info",
			type="rich",
			color=0x110a25
		)

		embed.set_thumbnail(url=self.client.user.avatar)
		embed.description = "Rikka Takanashi bot."
		embed.add_field(name="Created by", value=f'[MrDatrkHooD](https://mrdarkhood.dev/)')
		embed.add_field(name="Framework", value="[discord.py %s](http://discordpy.readthedocs.io/en/latest/)"% discord.__version__)

		await ctx.reply(ctx.channel, embed=embed)

	@commands.command(brief=f'Gives info about server that runs this bot.')
	async def botSrv(self, ctx):
		embed = discord.Embed(
			title="Server info",
			type="rich",
			color=0xd70a53
		)

		embed.set_thumbnail(url="https://rikka.us/debian.png")

		embed.add_field(name="Operating system", value=platform.system(), inline=False)
		embed.add_field(name="Release", value=platform.release(), inline=False)
		embed.add_field(name="Distro", value=distro.linux_distribution(), inline=False)

		uptime = display_time(time.time() - psutil.boot_time(), 6)

		embed.add_field(name="Uptime", value=uptime, inline=False)
		embed.add_field(name="CPU Usage", value=f"{psutil.cpu_percent()}%")
		embed.add_field(name="RAM Usage", value=f"{psutil.virtual_memory().percent}%")
		embed.add_field(name="Machine", value=platform.machine())
		embed.add_field(name="Platform", value=platform.platform())
		embed.add_field(name="Version", value=platform.version())
		embed.add_field(name="Mac version", value=platform.mac_ver())
		embed.set_footer(text=f"Made using discord.py", icon_url="https://www.python.org/static/opengraph-icon-200x200.png",)

		await ctx.reply(ctx.channel, embed=embed)

	@commands.command(brief=f'Links regarding this bot.')
	async def botUrls(self, ctx):
		embed = discord.Embed(
			title="Useful links",
			type="rich",
			color=0xd70a53
		)

		for urlName, url in config.urls:
			embed.add_field(name=urlName, value=url, inline=False)

		await ctx.reply(embed=embed, mention_author=False)

async def setup(client):
	await client.add_cog(botInfo(client))
