import discord
from discord.ext import commands
import sqlite3

class Invite(commands.Cog):
	def __init__(self, client):
		self.client = client
		self.db = sqlite3.connect('bot.db')
		

	@commands.command(brief="Create invite",
					  description="Sends one-time-use invite link to server for requester by dm.",
					  max_age=43200)
	async def invite(self, ctx):
		guild_id = str(ctx.guild.id)
		channel_id = str(ctx.channel.id)
		c = self.db.cursor()

		c.execute("SELECT 1 FROM disabled_commands_per_channel WHERE guild_id = ? AND channel_id = ? AND command = ?",
				  [ctx.guild.id, ctx.channel.id, 'invite'])

		if c.fetchone():
			c.close()
			return

		
		c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [ctx.guild.id])
		log_channel = c.fetchone()[0]

		invite = await ctx.channel.create_invite(max_uses = 1, max_age=43200)

		c.execute("INSERT INTO invites (guild_id, channel_id, inviter, invite_code) VALUES (?, ?, ?, ?)",
				  [ctx.guild.id, ctx.channel.id, ctx.message.author.id, invite.code])
		self.db.commit()
		c.close()
		
		await ctx.message.author.send("Here is your one-time-use invite link:\r\n" + invite.url)
		await ctx.reply("I've sent you a private message.")
		
		await self.client.get_channel(log_channel).send(ctx.author.name + " created invite " + invite.code)
		

	@commands.Cog.listener()
	async def on_invite_create(self, ctx):
		if ctx.inviter == self.client.user:
			return

		c = self.db.cursor()
		c.execute("SELECT log_channel, user_invites_allowed FROM guild_settings WHERE guild_id = ?", [ctx.guild.id])
		log_channel, user_invites_allowed = c.fetchone()
		logChannel = self.client.get_channel(log_channel)

		if not user_invites_allowed:
			await self.client.delete_invite(ctx)
			await logChannel.send(ctx.inviter.mention + " made invite but I deleted it.")
		else:
			c.execute("INSERT INTO invites (guild_id, channel_id, inviter, invite_code) VALUES (?, ?, ?, ?)",
					  [ctx.guild.id, ctx.channel.id, ctx.inviter.id, ctx.code])
			self.db.commit()
			await logChannel.send(ctx.code + " invite created by " + ctx.inviter.id)
			
		c.close()


		
	@commands.Cog.listener()
	async def on_invite_delete(self, ctx):
		
		guild_id = str(ctx.guild.id)
		channel_id = str(ctx.channel.id);
		c = self.db.cursor()
		c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [ctx.guild.id])
		logChannel = self.client.get_channel(c.fetchone()[0])
		
		c.execute("SELECT inviter FROM invites WHERE guild_id = ? AND invite_code = ?", [ctx.guild.id, ctx.code])
		inviterid = c.fetchone()
		try:
			inviter = ctx.guild.get_member(inviterid[0]).name + " (user id: " + str(inviterid[0]) + ")"
		except:
			if inviterid:
				inviter = f"deleter user with ID {inviterid[0]}"
			else:
				inviter = "unknown"
		c.execute("DELETE FROM invites WHERE invite_code = ? AND guild_id = ?", [ctx.code, guild_id])
		self.db.commit()
		c.close()
#		c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [guild_id])
#		log_channel = c.fetchone()[0]
#		c.execute("SELECT inviter FROM invites WHERE guild_id = ? AND invite_code = ?", [guild_id, ctx.code])
#		inviter = c.fetchone()[0]
#		c.close()
		
		await logChannel.send(f"Invite {ctx.code}, according to my database; created by {inviter}, got deleted.", mention_author=False)

	
async def setup(client):
	await client.add_cog(Invite(client))
