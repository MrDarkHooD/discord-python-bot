import discord
from discord.ext import commands, tasks

import asyncio
import sqlite3
import datetime
import croniter
import aiohttp
import io
import pytz
import re
import random

class Cron(commands.Cog):

	
	def __init__(self, client):
		self.client = client
		self.db = sqlite3.connect('bot.db')
			
		self.cron.start()
		
	async def cog__unload(self):
		self.cron.cancel()
		pass
		
	# Function to replace variable matches with their values
	def replace(match):
		var_name = match.group(1)
		return str(variables.get(var_name, match.group(0)))  # Replace with value or original if not found

		
	@tasks.loop(seconds=60)
	async def cron(self):
		timeNow = datetime.datetime.now()

		print("cron trigger at " + str(timeNow));
		c = self.db.cursor()
		c.execute("SELECT guild_id, channel_id, cronTime, text, file, filename FROM cron", [])
		jobs = c.fetchall()
		c.close()

		for job in jobs:
			guild_id, channel_id, cronFormat, text, fileURL, filename = job
			jobGuild = self.client.get_guild(guild_id)
			jobChannel = self.client.get_channel(channel_id)

			cron = croniter.croniter(cronFormat, timeNow)
			

			# Do not ask, i have no fucking clue
			if croniter.croniter(cronFormat, timeNow).match(cronFormat, timeNow):
				
				variables = {
					"year": datetime.datetime.now().year,
					"month": datetime.datetime.now().month,
					"day": datetime.datetime.now().day,
					"hour": datetime.datetime.now().hour,
					"minute": datetime.datetime.now().minute,
					"second": datetime.datetime.now().second,
					"randomint": random.randint(1, 1000)  # Generate a random integer
				}

				# Regular expression to match variables enclosed in {}
				pattern = r'\{([^{}]*)\}'
				
				
				matches = re.finditer(pattern, text)

				# Iterate over matches and replace them with their values
				replaced_str = ""
				prev_end = 0
				for match in matches:
					var_name = match.group(1)
					if var_name in variables:
						replaced_str += text[prev_end:match.start()] + str(variables[var_name])
					else:
						replaced_str += text[prev_end:match.end()]
					prev_end = match.end()

				# Append the remaining part of the input string
				replaced_str += text[prev_end:]
	
				text = replaced_str
	
				if fileURL != "":
					async with aiohttp.ClientSession() as session: # creates session
						async with session.get(fileURL) as resp: # gets image from url
							img = await resp.read() # reads image from response
							with io.BytesIO(img) as file: # converts to file-like object
								await jobChannel.send(text, file=discord.File(file, filename))
				else:
					await jobChannel.send(text)

		return

	@commands.command()
	@commands.has_permissions(administrator = True)
	async def listCrons(self, ctx):
		c = self.db.cursor()
		c.execute("SELECT id, channel_id, cronTime, text, file FROM cron WHERE guild_id = ?", [ctx.guild.id])
		jobs = c.fetchall()
		c.close()

		desc = ""
		
		for job in jobs:
			
			cronId, channelId, cronTime, text, file = job
			desc += "* " + str(cronId) + ". " + str(cronTime) + " " + str(text) + "\n"
			

		embed = discord.Embed(
			title = "Crons",
			description = desc,
			color = 0x3498db
		)
		#embed.set_thumbnail(url=file)
		#embed.add_field(name="Target channel", value=str(ctx.guild.get_channel(channelId)), inline=False)
		#embed.add_field(name="Scheduling parameters", value="```" + str(cronTime) + "```", inline=True)
		#embed.add_field(name="Message", value=str(text), inline=True)
			
			
		await ctx.reply(embed=embed)

	@commands.command()
	@commands.has_permissions(administrator = True)
	async def deleteCron(self, ctx, cronId):
		c = self.db.cursor()
		c.execute("DELETE FROM cron WHERE guild_id = ? AND id = ?", [ctx.guild.id, cronId])
		self.db.commit()
		c.close()
		await ctx.reply("Cron deleted")
		
	@commands.command()
	@commands.has_permissions(administrator = True)
	async def reminder(self, ctx, time, text, isDaily = False):
		#c = self.db.cursor()
		#c.execute("INSERT INTO blööh user_id, time, text", [ctx.guild.id, cronId])
		#self.db.commit()
		#c.close()
		await ctx.reply("ei")
	
	@cron.before_loop
	async def beforeCron(self):
		await self.client.wait_until_ready()
	

	
async def setup(client):
   	await client.add_cog(Cron(client))
