import discord
from discord.ext import commands
from languages import en as lang
import config

import urllib
import json

class Pubchem(commands.Cog):
	def __init__(self, bot):
		self.client = bot

	@commands.command(description="Returns Pubchem data for given chemical")
	async def property(self, ctx, *, compound):
		imageURL = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/"+compound+"/PNG?record_type=2d&image_size=300x300"
		compoundURL = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/"+compound+"/property/MolecularFormula,HBondAcceptorCount,HBondDonorCount,IUPACName,Title,IsomericSmiles,CanonicalSMILES,MolecularWeight/JSON"

		#https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/urea/synonyms/JSON
		
		with urllib.request.urlopen(compoundURL) as response:
			html = response.read()
			chemData = json.loads(html)['PropertyTable']['Properties'][0]

			embed = discord.Embed(
				title = chemData["Title"],
				color=discord.Color.orange()
			)

			embed.set_thumbnail(url=imageURL) 
			embed.add_field(name="Molecular formula", value=str(chemData['MolecularFormula']))
			embed.add_field(name="Molecular weight", value=str(chemData['MolecularWeight']))
			embed.add_field(name="IUPAC name", value=str(chemData['IUPACName']))
			embed.add_field(name="Isomeric SMILES", value=chemData['IsomericSMILES'])
			embed.add_field(name="Canonical SMILES", value=chemData['CanonicalSMILES'])
			
			embed.add_field(name="Hydrogen-bond donors", value=str(chemData['HBondDonorCount']))
			embed.add_field(name="Hydrogen-bond acceptors", value=str(chemData['HBondAcceptorCount']))
			
			embed.set_footer(text="Powered by https://pubchem.ncbi.nlm.nih.gov/")

			await ctx.reply(embed=embed, mention_author=False)
			return
		await ctx.reply("Failed to fetch data")
			
async def setup(bot):
	await bot.add_cog(Pubchem(bot))
