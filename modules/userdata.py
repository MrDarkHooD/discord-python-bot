import discord
from discord.ext import commands

from dateutil.relativedelta import relativedelta
import datetime
import pytz
import sqlite3
import requests



class Userdata(commands.Cog):
	def __init__(self, client):
		self.client = client
		self.db = sqlite3.connect('bot.db')

	@commands.command(
		brief="Get user avatar",
		description="Returns link to avatar of given user")
	async def user_avatar(self, ctx, target: discord.User):
		try:
			request = requests.get(target.avatar) #Here is where im getting the error
			if request.status_code == 200:
				await ctx.send(target.avatar)
			else:
				await ctx.send("Failed to get avatar.\nStatus code " + request.status_code)
		except:
			await ctx.send("Failed to get avatar")

	@commands.command(brief="Get user age", description="Returns registration date and time of given user")
	async def user_age(self, ctx, target: discord.User):
		today = datetime.datetime.now()
		target_timezone = pytz.timezone('Europe/Helsinki')
		today = target_timezone.localize(today)

		delta = relativedelta(today, target.created_at)
		age_string = f"Account {target.display_name} is {delta.years} years, {delta.months} months, and {delta.days} days old."
		await ctx.send(age_string)
		return

	@commands.command( brief="Shows member card" )
	@commands.guild_only()
	async def member_card(self, ctx, user: discord.User = None):
		c = self.db.cursor()
		
		if user == None:
			user = ctx.author

		c.execute("""
			SELECT 
				u1.xp_amount,
				u1.rank_lvl,
				u1.message_count,
				u1.voice_minutes,
				bonus_xp,
				(
					SELECT COUNT(*) 
						FROM user_xp AS u2 
						WHERE u2.xp_amount > u1.xp_amount AND left = 0
				) + 1 AS rank_pos,
				r.xp AS next_level_xp,
				r.role_id AS current_level_role_id
			FROM
				user_xp AS u1
			LEFT JOIN
				rank_levels AS r ON u1.guild_id = r.guild_id
				AND r.level_number = u1.rank_lvl
			WHERE 
				u1.guild_id = ? 
				AND u1.user_id = ? 
				AND u1.left = 0
		""", [ctx.guild.id, user.id])
		
		result = c.fetchone()
		c.close()
		
		if not result:
			await ctx.reply("Member not found")
			return
			
		xpCurrent, userLevel, messageCount, minutes, xpBonus, rankPos, xpNextLevel, roleId = result
		voiceHours = f"{minutes // 60}.{minutes % 60}"
		
		if not xpNextLevel:
			percent_to_next = "Max lvl reached"
		else:
			percent_to_next = str(round( ( xpNextLevel - xpCurrent ) / xpNextLevel * 100, 2 ))+"%"
		
		role = ctx.guild.get_role(roleId)

		embed = discord.Embed(
			title = str(user) + "'s member card",
			color = 0xffcd01
		)

		member  = ctx.guild.get_member(user.id)
		
		embed.set_thumbnail(url=user.avatar)
		embed.add_field(name="Level", value=f"{role}, lvl {userLevel}", inline=False)
		embed.add_field(name="XP", value=xpCurrent, inline=True)
		embed.add_field(name="% XP to next level", value=percent_to_next, inline=True)
		embed.add_field(name="Total messages", value=messageCount, inline=True)
		embed.add_field(name="Voice hours", value=voiceHours, inline=True)
		embed.add_field(name="Bonus xp", value=xpBonus, inline=True)
		embed.add_field(name="n'th in xp stats", value=str(rankPos), inline=True)
		embed.add_field(name="Joined server", value=member.joined_at, inline=True)
		#embed.add_field(name="icon", value=str(role.display_icon), inline=True) # This might be something that requires high boost level

		await ctx.reply(embed=embed, mention_author=False)

async def setup(client):
	await client.add_cog(Userdata(client))
