import discord
from discord.ext import commands
import config
from languages import en as lang

import urllib
import json
import re

class IMDB(commands.Cog):
	def __init__(self, client):
		self.client = client

	@commands.command(name="imdb",
					  description="Returns IMDB data for given movie or serie from OMDB api ")
	async def imdb(self, ctx, *, arg):


		m = re.compile("^(.*) (\d{4})$").fullmatch(arg)
		if m:
			movie = urllib.parse.quote_plus(m.group(1))
			year = m.group(2)
		else:
			year = 0
			movie = urllib.parse.quote_plus(arg)

		searchtype = "t"
		if re.compile('^tt\d+$').search(arg):
			searchtype = "i"
		omdbURL = "http://www.omdbapi.com/?" + searchtype + "=" + movie + "&apikey=" + config.api['omdb']
		if int(year) > 0:
			omdbURL = omdbURL + "&y=" + str(year)

		async with ctx.typing():
			try:
				with urllib.request.urlopen(omdbURL) as response:
					html = response.read()
					omdbData = json.loads(html)

					if omdbData == None:
						await ctx.reply("Not found")
						return
						
					print(omdbData)
					
					embed = discord.Embed(
						title = omdbData["Title"],
						url = "https://www.imdb.com/title/" + omdbData['imdbID'] + "/",
						color=0xffff00
					)

					if omdbData['Poster'] == "N/A":
						omdbData['Poster'] = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png"

					embed.set_author(name=str(omdbData['Director'])) #, icon_url=omdbData['Poster'])
					embed.set_thumbnail(url=omdbData['Poster'])
#					embed.set_image(url=omdbData['Poster'])
					if omdbData['Released'] is not None:
						embed.add_field(name="Released", value=omdbData['Released'], inline=True)
					if omdbData['Runtime'] is not None:
						embed.add_field(name="Runtime", value=omdbData['Runtime'], inline=True)
					if omdbData['Genre'] is not None:
						embed.add_field(name="Genre", value=omdbData['Genre'], inline=True)
					if omdbData['Director'] is not None:
						embed.add_field(name="Director", value=omdbData['Director'], inline=True)
					if omdbData['Writer'] is not None:
						embed.add_field(name="Writer", value=omdbData['Writer'], inline=True)
					if omdbData['Actors'] is not None:
						embed.add_field(name="Actors", value=omdbData['Actors'], inline=True)
					if omdbData['Plot'] is not None:
						embed.add_field(name="Plot", value="||" + omdbData['Plot'] + "||", inline=False)
					
					ratingList = ""
					for rating in omdbData['Ratings']:
						ratingList += rating["Source"] + ": " + rating["Value"] + "\n"
						
					if  ratingList is not "":
						embed.add_field(name="Ratings", value=ratingList, inline=False)
						
					await ctx.reply(embed=embed)
			except urllib.error.HTTPError as e:
				await ctx.reply("Movie/serie not found")

async def setup(client):
	await client.add_cog(IMDB(client))
