import discord
from discord.ext import commands

from random import randint

class Random(commands.Cog):
	
	def __init__(self, client):
		self.client = client

	@commands.command(brief="Get random number",
					  description="Gives random numbers, !roll x y",
					  help="!roll smallest_possible_number biggest_possible_number")
	async def roll(self, ctx, min = 0, max = 10):
		await ctx.reply(randint(int(min), int(max)))

	@commands.command(brief="Get boolean", description="Gives 0 or 1")
	async def bool(self, ctx):
		await ctx.reply(randint(0, 1))

	@commands.command(brief="Choose",
					  description="Gives random choise, !choose x y z...",
					  help="!choose x y z...\nrandomly selects one of options.")
	async def choose(self, ctx, * options):
		await ctx.reply(options[randint(0, len(options)-1)])

	@commands.command(brief="Throw dice",
					  description="Gives random numbers, !dice XdY",
					  help="!dice xdy Where x is number of dices and y is number of sides.")
	async def dice(self, ctx, arguments = "1d6"):
		dice_data = arguments.split("d")
		dices = int(dice_data[0])
		sides = int(dice_data[1])

		if dices > 100:
			await ctx.reply("I only have 100 dices.")
			return
		if sides > 1000:
			await ctx.reply("Dat is one big ass dice.")
			return

		results = []
		for x in range(0, dices):
			results.append(randint(1, sides))

		embed = discord.Embed(
			title = "Dice roll",
			color=0xff0000
		)
		embed.set_thumbnail(url="https://rikka.us/dice.png")

		if dices > 1:
			embed.add_field(name="Results", value=", ".join([str(result) for result in results]), inline=True)

		embed.add_field(name="Total sum: ", value=sum(results), inline=False)

		await ctx.reply(embed=embed)

async def setup(client):
	await client.add_cog(Random(client))
