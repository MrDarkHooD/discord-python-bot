import discord
from discord.ext import commands
import config

import time
import sqlite3

class Log(commands.Cog):
	def __init__(self, client):
		self.client = client
		self.concealExistance = True
		self.db = sqlite3.connect('bot.db')

	@commands.Cog.listener()
	async def on_message(self, message):
		if message.author == self.client.user:
			return
		
		# message.reference is not None:

		files = ", ".join([str(attachment.url) for attachment in message.attachments])
		
		if message.guild:
			guild = message.guild.id
		else:
			guild = message.author.id


		c = self.db.cursor()
		c.execute("INSERT INTO message_log (guild_id, user_id, message_id, content, file, timestamp) VALUES (?, ?, ?, ?, ?, ?)", [guild, message.author.id, message.id, message.content, files, int(time.time())])
		self.db.commit()
		c.close()

async def setup(client):
	await client.add_cog(Log(client))
