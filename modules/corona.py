import discord
from discord.ext import commands

import urllib
import json

class Corona(commands.Cog):
	def __init__(self, client):
		self.client = client

	@commands.command(brief="Total of covid infections known to man")
	async def covid19(self, ctx):
		with urllib.request.urlopen("https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json") as response:
			html = response.read()
			thlData = json.loads(html)
			tartunnat = 0
			for tartunta in thlData["dataset"]["value"]:
				tartunnat = tartunnat + int(tartunta)
			await ctx.send(str(tartunnat))

async def setup(client):
	await client.add_cog(Corona(client))
