import discord
from discord.ext import commands, tasks
from discord.utils import get

import pytz
import requests
import mimetypes
import sqlite3
import time as neekeri
import pandas as pd
from datetime import time, datetime, timedelta, timezone
import sys
import os

from config import filetypes, ownerID

import termcolor
from termcolor import colored

from colors import *

class Rank(commands.Cog):
	
	def __init__(self, client):
		self.client = client
		self.db = sqlite3.connect('bot.db')
		self.voiceMinuteAdder.start()
		
	def cog_unload(self):
		if self.voiceMinuteAdder:
			self.voiceMinuteAdder.cancel()
			
		pass

	@commands.command(hidden=True)
	@commands.guild_only()
	@commands.has_permissions(administrator = True)
	async def rankUpdate(self, ctx, levelfactory = 50):
		# Broken, decapricated, shit
		return
		c = self.db.cursor()
		c.execute("DELETE FROM rank_levels WHERE guild_id = ?", [ctx.guild.id])
		self.db.commit()

		required_xp = 0
		
		for x, role in enumerate([i for i in ctx.guild.roles]):
			required_xp += levelfactory * x
			c.execute("INSERT INTO rank_levels (guild_id, role_id, xp, level_number) VALUES (?, ?, ?, ?)", [ctx.guild.id, role.id, required_xp, x])

		self.db.commit()
		await ctx.send("Roles updated to database")
		c.close()

	@commands.command(hidden=True)
	@commands.guild_only()
	@commands.has_permissions(administrator = True)
	async def addRank(self, ctx, role_id, position = None):
		return
		
		c.execute("SELECT xp as biggestXP, level_number as biggestLVL FROM rank_levels WHERE guild_id = ? ORDER BY level_number DESC LIMIT 1", [ctx.guild.id])
		biggest = c.fetchone()
		
		newXP += levelfactory * biggest[1]
		
		c.execute("INSERT INTO rank_levels (guild_id, role_id, xp, level_number) VALUES (?, ?, ?, ?)", [ctx.guild.id, role_id, newXP, biggest+1])

		
	@commands.command( brief="Top list")
	@commands.guild_only()
	async def topRank(self, ctx, startPos = 0):
		startPos *= 30

		rankList = ""
		c = self.db.cursor()
		c.execute("SELECT xp_amount as xp, rank_lvl as lvl, user_id as uid FROM user_xp WHERE guild_id = ? AND left = 0 ORDER BY xp_amount DESC LIMIT 30 OFFSET ?", [ctx.guild.id, startPos-30])
		for value in c.fetchall():
			xpAmount, lvlNum, userID = value
			rankList += f"> <@{userID}> {xpAmount}xp, {lvlNum}lvl\n"
			
		embed = discord.Embed(
			title = "Top ranks",
			description=rankList,
			color = 0xffcd01
		)
		embed.set_thumbnail(url="https://rikka.us/img/top.png")
		await ctx.reply(embed=embed)
		c.close()

	@commands.command( brief="Freezes or unfreezes your xp" )
	@commands.guild_only()
	async def freeze_xp(self, ctx):
		c = self.db.cursor()
		c.execute("SELECT frozen FROM user_xp WHERE guild_id = ? AND user_id = ?", [ctx.guild.id, ctx.author.id])
		isfrozen = c.fetchone()[0]
		
		if isfrozen == None:
			c.close()
			await ctx.reply("You are ghost, go away.")
			return
		
		isfrozen = not isfrozen
		await ctx.reply("Frozen status: " + str(isfrozen))
		
		c.execute("UPDATE user_xp SET frozen = ? WHERE guild_id = ? AND user_id = ?", [int(isfrozen), ctx.guild.id, ctx.author.id])
		self.db.commit()
		c.close()
		return
		
	@commands.command( brief="Give xp to user")
	@commands.guild_only()
	@commands.has_permissions(administrator = True)
	async def give_xp(self, ctx, user: discord.User, amount: int):
		if amount > 1000000:
			await ctx.reply("You cannot give more than 1.000.000xp at once.")
			return

		c = self.db.cursor()
		c.execute("SELECT frozen FROM user_xp WHERE guild_id = ? AND user_id = ?", [ctx.guild.id, user.id])
		if c.fetchone() == None:
			c.close()
			await ctx.reply("User not in database.")
			return

		if c.fetchone():
			c.close()
			await ctx.reply(user.name + " has frozen their xp.")
			return
		
		c.execute("UPDATE user_xp SET xp_amount = xp_amount + ?, bonus_xp = bonus_xp + ? WHERE guild_id = ? AND user_id = ?",
				  [amount, amount, ctx.guild.id, user.id])
		self.db.commit()
		c.close()
		await ctx.reply(user.name + " got " + str(amount) + "xp.")
		return
			

	@commands.command( brief="Remove xp from user")
	@commands.guild_only()
	@commands.has_permissions(administrator = True)
	async def remove_xp(self, ctx, user: discord.User, amount: int):

		c = self.db.cursor()
		c.execute("SELECT 1 FROM user_xp WHERE guild_id = ? AND user_id = ?", [ctx.guild.id, user.id])
		user_xp = c.fetchone()
			
		if user_xp == None:
			c.close()
			await ctx.reply("User not in database.")
			return

		user_xp = int(user_xp)

		if user_xp - amount < 0:
			amount = user_xp
			
		c.execute("UPDATE user_xp SET xp_amount = xp_amount - ? WHERE guild_id = ? AND user_id = ?",
				  [amount, ctx.guild.id, user.id])
		self.db.commit()
		c.close()
		await ctx.reply(str(user.mention) + " lost " + str(amount) + "xp.")
		return

	@commands.Cog.listener()
	async def on_member_join(self, ctx):
		print(f"User id: {ctx.id}")
		print(f"Guild id: {ctx.guild.id}")
		c = self.db.cursor()
		c.execute("SELECT xp_amount FROM user_xp WHERE guild_id = ? AND user_id = ?", [ctx.guild.id, ctx.id])
		xp = c.fetchone()
		if xp:
			c.execute("UPDATE user_xp SET left = 0 WHERE id = ?", [ctx.id])
			self.db.commit()
			xp = xp[0]
		else:
			xp = 0
			

		c.execute("INSERT INTO user_xp (guild_id, user_id, xp_amount, frozen) VALUES (?, ?, ?, ?)", [ctx.guild.id, ctx.id, 0, 0])
		self.db.commit()
		
		c.execute("SELECT role_id FROM rank_levels WHERE xp <= ? AND guild_id = ? ORDER BY xp DESC LIMIT 1", [xp, ctx.guild.id])
		role = ctx.guild.get_role(c.fetchone()[0])
		await ctx.add_roles(role)
		c.close()

	@commands.Cog.listener()
	async def on_message(self, message):
		if message.author.bot:
			return

		if message.author == self.client.user:
			return
		
		if not message.guild:
			return

		c = self.db.cursor()
		c.row_factory = sqlite3.Row
		c.execute("""
			SELECT
				xp_from_message as message,
				xp_from_image as image,
				xp_from_video as video,
				xp_from_sound as sound,
				xp_from_code as code,
				xp_from_text as text,
				xp_from_document as document,
				xp_from_application as application,
				xp_from_boost as boost
			FROM
				guild_settings
			WHERE
				guild_id = ?
			""",
				  [message.guild.id]) 
		xpFrom = c.fetchone()

		if xpFrom == None:
			return

		c.execute("SELECT xp_amount, frozen FROM user_xp WHERE guild_id = ? AND user_id = ?", [message.guild.id, message.author.id])
		dbdata = c.fetchone()
		
		if dbdata == None:
			c.close()
			return
		
		xpNow, isFrozen = dbdata	

		c.execute("UPDATE user_xp SET message_count = message_count + 1 WHERE guild_id = ? AND user_id = ?", [message.guild.id, message.author.id])
		self.db.commit()
		
		#stops xp gain if user is frozen
		if isFrozen:
			c.close()
			return
		
		if message.type == discord.MessageType.premium_guild_subscription:
			# We do this now because next if-statement might fuck things up
			c.execute("UPDATE user_xp SET xp_amount = xp_amount + ?, bonus_xp = bonus_xp + ? WHERE guild_id = ? AND user_id = ?",
					  [xpFrom["boost"], xpFrom["boost"], message.guild.id, message.author.id])
			self.db.commit()
			
			c.execute("SELECT main_channel FROM guild_settings WHERE guild_id = ?", [message.guild.id])
			await self.client.get_channel(c.fetchone()[0]).send(f"{ctx.mention} boosted! Extra {xpFrom['boost']}xp given to you!")

		c.execute("SELECT channel_id FROM rank_disabled_channels WHERE guild_id = ?", [message.guild.id])
		if message.channel.id in c.fetchall():
			c.close()
			return

		## The fuck is this piece of shit?
		#time limiter
		c.execute("SELECT timestamp FROM message_log WHERE guild_id = ? AND user_id = ? AND message_id IS NOT ? ORDER BY id DESC LIMIT 1", [message.guild.id, message.author.id, message.id])
		try:
			last_time_message_xp = c.fetchone()[0]
		except NameError:
			last_time_message_xp = int(neekeri.time()) - 6

		if last_time_message_xp + 5 > int(neekeri.time()): #needs to be changed into db query
			c.close()
			return
		
		if len(message.content) > 3:
			xpNow += xpFrom["message"]

		if len(message.attachments) > 0:
			for attachment in message.attachments:
				response = requests.get(attachment.url)
				# q&d to remove 'content-type: text/plain; charset=utf-8' tier
				mimetype = str(response.headers['content-type']).split(';')[0]
				
				# diarrhea to detect filetypes in future
				mimetypeFound = False
				
				for fileType, mimelist in filetypes.items():
					if mimetype in mimelist:
						xpNow += xpFrom[fileType]
						mimetypeFound = True # liquid poop
						break

				# more of that diarrhea
				if not mimetypeFound: 
					xpNow += xpFrom["message"]
					ctx.message.author.send
					await message.guild.get_member(ownerID).send("Detected unregistered mimetype: " + mimetype)

			
		c.execute("UPDATE user_xp SET xp_amount = ? WHERE guild_id = ? AND user_id = ?", [xpNow, message.guild.id, message.author.id])
		self.db.commit()
		
		
		## Now starts level update part if needed
		c.execute("SELECT xp_amount as xp, rank_lvl as level FROM user_xp WHERE user_id = ? AND guild_id = ? ORDER BY id DESC LIMIT 1", [message.author.id, message.guild.id])
		userData = c.fetchone()
		
		c.execute("SELECT role_id as id, xp, level_number as level FROM rank_levels WHERE xp >= ? AND guild_id = ? ORDER BY xp ASC LIMIT 1", [userData["xp"], message.guild.id])
		nextRole = c.fetchone()

		# Top level gained already
		if nextRole == None:
			c.close()
			return
		
		# Bad name for variable, it actually checks if user level matches level user xp is linked to
		if nextRole["level"] != userData["level"]:
			#Remove all rank related roles from user, just to be sure
			c.execute("SELECT role_id FROM rank_levels WHERE guild_id = ?", [message.guild.id])
			allRankRoles = [row[0] for row in c.fetchall()]
			for role in message.author.roles:
				if role.id in allRankRoles:
					await message.author.remove_roles(message.guild.get_role(role.id))

			await message.author.add_roles(message.guild.get_role(nextRole["id"]))

			c.execute("UPDATE user_xp SET rank_lvl = ? WHERE guild_id = ? AND user_id = ?",
					  [nextRole["level"], message.guild.id, message.author.id])
			self.db.commit()
				
			embed = discord.Embed(
				title = "Level up!",
				description = f"{message.author.mention} leveled up!\nNew level is {nextRole['level']}.\nYou have been granted {message.guild.get_role(nextRole['id'])} role.",
				color = 0xffcd01
			)
			c.execute("SELECT main_channel FROM guild_settings WHERE guild_id = ?", [message.guild.id])
			await self.client.get_channel(c.fetchone()[0]).send(embed=embed)

		c.close()

	@tasks.loop(minutes=1)
	async def voiceMinuteAdder(self):
		timeNow = datetime.now()
		print("voice sniffer triggered at " + str(timeNow));
		
		c = self.db.cursor()
		
		for guild in self.client.guilds:
			
			c.execute("SELECT xp_from_voice FROM guild_settings WHERE guild_id = ?", [guild.id])
			xpGain = c.fetchone()[0]

			for voiceChannel in guild.voice_channels:

				# Ugly piece of shit because i have no time to figure this shit out
				c = self.db.cursor()
				c.execute("SELECT 1 FROM disabled_cogs_per_channel WHERE guild_id = ? AND channel_id = ? AND cog = ?", [guild.id, voiceChannel.id, "Rank"])
				if c.fetchone():
					c.close()
					continue
					
				voiceMembers = voiceChannel.voice_states.keys()
				if(len(voiceMembers) == 0):
					continue

				print(color("\U0001F50A", 'orange') + " " + colored(voiceChannel.name, 'white', attrs=['bold']) + " (" + colored(str(guild), 'blue') + ")")

				for member in voiceMembers:
					member = guild.get_member(member)
					
					if(len(voiceMembers) == 1):
						xpGain = 0
					
					if member.voice.self_mute:
						muteText = color("M", 'red')
					else:
						muteText = "\U0001F3A4"
						
					if member.voice.self_deaf:
						xpGain = 0
						deafText = color("D", 'red')
					else:
						deafText = "\U0001F3A7"
						
					print(f"\t{member} {muteText} {deafText}")
					

					c.execute("UPDATE user_xp SET voice_minutes = voice_minutes + 1, xp_amount = xp_amount + ? WHERE guild_id = ? AND user_id = ?",
							  [xpGain, guild.id, member.id])
					self.db.commit()
		
		c.close()

	@tasks.loop(minutes=1)
	async def passAndDec(self):
		timeNow = datetime.now()
		
		if timeNow.strftime("%H:%M") != "00:00":
			return
		
#		print("XP decader and passiver triggered at " + str(timeNow));
		
#		for guild in self.client.guilds:
#			c.execute("SELECT passive_time, decay_time, xp_from_passive, xp_from_decay WHERE guild_id = ?",
#				  [guild.id]) 
#			guildData = c.fetchone()
#			if not fuildData:
#				pass
			
#			for member in guild.members:
				
		#+10xp for 20d
		#-5xp after 30d
		#0xp >=100d -> kick
		
	@voiceMinuteAdder.before_loop
	async def beforeSniffing(self):
		await self.client.wait_until_ready()

async def setup(client):
	await client.add_cog(Rank(client))
