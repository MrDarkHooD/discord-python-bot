import discord
from discord.ext import commands
import config

import time
import sqlite3

class ServerLog(commands.Cog):
	def __init__(self, client):
		self.client = client
		self.db = sqlite3.connect('bot.db')
		self.concealExistance = True

	@commands.Cog.listener()
	async def on_message(self, ctx):
		if ctx.type == discord.MessageType.premium_guild_subscription:
			c = self.db.cursor()
			c.execute("SELECT log_channel, voice_log_channel FROM guild_settings WHERE guild_id = ?", [str(ctx.guild.id)])
			logChannel = self.client.get_channel(c.fetchone()[0])
			
			embed = discord.Embed(
				title = "Server boosted",
				color = 0xffd700
			)
			
			embed.add_field(name="Booster", value=ctx.mention, inline=True)
			embed.add_field(name="Boost level now", value=ctx.guild.premium_subscription_count, inline=True)
			
			logChannel.send(embed=embed)
		
	@commands.Cog.listener()
	async def on_message_delete(self, message):
		c = self.db.cursor()
		c.execute("SELECT log_channel, voice_log_channel FROM guild_settings WHERE guild_id = ?", [str(message.guild.id)])
		logChannel = self.client.get_channel(c.fetchone()[0])

		embed = discord.Embed(
			title = "Message deleted",
			color = 0xffa500
		)

		user_image = message.author.avatar
		embed.set_author(name=str(message.author), icon_url=user_image)
		embed.add_field(name="Message", value=message.content, inline=True)
		embed.add_field(name="Channel", value=message.channel.mention, inline=True)
		embed.add_field(name="Author", value=message.author.id, inline=True)

		#dirty hack to get data if admin deleted message
		#this will also trigger in case if user deletes own message
		#but last audit log action tells that moderator deleted
		#message from that same user
		async for entry in message.guild.audit_logs(limit=1):
			if str(entry.action) == "AuditLogAction.message_delete" and message.author == entry.target:
				embed.add_field(name="Deleter (maybe)", value=entry.user, inline=True)

		embed.add_field(name="Channel id", value=message.channel.id, inline=True)

		await logChannel.send(embed=embed)

	@commands.Cog.listener()
	async def on_member_join(self, ctx):
		c = self.db.cursor()
		c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [ctx.guild.id])
		logChannel = self.client.get_channel(c.fetchone()[0])
		
		## invite.py deletes record from database, but it also post's it to log channel.
		#c.execute("SELECT id, invite_code, inviter FROM invites WHERE guild_id = ?", [ctx.guild.id])
		#dbInvites = c.fetchall()
		#invites = await ctx.guild.invites()
		c.close()

		embed = discord.Embed(
			title = "User joined",
			color = 0x00ff00
		)

		embed.set_author(name=str(ctx), icon_url=ctx.avatar)
		embed.set_thumbnail(url=ctx.avatar)
		embed.add_field(name="Nick", value=ctx.name, inline=True)
		embed.add_field(name="Account created", value=ctx.created_at, inline=True)
		#embed.add_field(name="Invited by", value=ctx.inviter, inline=True)
		embed.add_field(name="User id", value=ctx.id, inline=True)

		await logChannel.send(embed=embed)

	@commands.Cog.listener()
	async def on_member_ban(self, data):
		c = self.db.cursor()
		c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [str(data.guild.id)])
		logChannel = self.client.get_channel(c.fetchone()[0])
		c.close()

		embed = discord.Embed(
			title = "User banned",
			color = 0xff0000
		)

		embed.set_author(name=str(data), icon_url=data.avatar)
		embed.set_thumbnail(url=data.avatar)
		embed.add_field(name="Nick", value=data.display_name, inline=True)
		embed.add_field(name="User id", value=data.id, inline=False)

		await logChannel.send(embed=embed)

	@commands.Cog.listener()
	async def on_member_unban(self, data):
		c = self.db.cursor()
		c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [str(data.guild.id)])
		logChannel = self.client.get_channel(c.fetchone()[0])
		c.close()

		embed = discord.Embed(
			title = "User unbanned",
			color = 0xff0000
		)

		embed.set_author(name=str(data), icon_url=data.avatar)
		embed.set_thumbnail(url=data.avatar)
		embed.add_field(name="Nick", value=data.display_name, inline=True)
		embed.add_field(name="User id", value=data.id, inline=False)

		await logChannel.send(embed=embed)

	@commands.Cog.listener()
	async def on_member_remove(self, member):
		c = self.db.cursor()
		c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [str(member.guild.id)])
		logChannel = self.client.get_channel(c.fetchone()[0])
		c.close()

		if member.guild is None:
			embed = discord.Embed(
				title = "User left",
				color = 0xff0000
			)
		else:
			embed = discord.Embed(
				title = "User kicked",
				color = 0xff0000
			)
			
			async for entry in member.guild.audit_logs(action=discord.AuditLogAction.kick):
				if entry.target == member:
					embed.add_field(name="Kicker", value=entry.user.mention, inline=False)
					break

		embed.set_author(name=str(member), icon_url=member.avatar)
		userRoles = ", ".join([role.mention for role in member.roles])
		embed.add_field(name="Roles", value=userRoles, inline=False)
		embed.add_field(name='DC Name:', value=member.mention, inline=False)
		
		await logChannel.send(embed=embed)

 #   @commands.Cog.listener()
 #   async def on_message_edit(self, old, new):
 #	   if old.content is new.content:
 #		   return
 #	   c = self.db.cursor()
 #	   c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [str(new.guild.id)])
 #	   log_channel = c.fetchone()[0]
 #	   c.close()

 #	   embed = discord.Embed(
 #		   title = "Message edited",
 #		   color = 0xff0000
 #	   )

 #	   embed.set_author(name=str(new.author), icon_url=new.author.avatar)
 #	   embed.add_field(name="Nick", value=new.author.display_name, inline=True)
 #	   embed.add_field(name="User id", value=new.author.id, inline=False)
 #	   embed.add_field(name="Channel", value=new.channel.name, inline=False)
 #	   embed.add_field(name="Old message", value=old.content, inline=False)
 #	   embed.add_field(name="New message", value=new.content, inline=False)

 #	   await self.client.get_channel(log_channel).send(embed=embed)


	@commands.Cog.listener()
	async def on_member_update(self, before, after):
		c = self.db.cursor()
		c.execute("SELECT log_channel FROM guild_settings WHERE guild_id = ?", [str(after.guild.id)])
		logChannel = self.client.get_channel(c.fetchone()[0])
		c.close()

		updates = [
			"nickname",
			"roles",
			"pending",
			"timeout",
			"guild_avatar",
			"flags"
		]
		
		#await logChannel.send(before.avatar.url)
		#await logChannel.send(before.avatar)
		#await logChannel.send(after.mention)
		#return
		
		embed = discord.Embed()
		description=None
		if before.display_name != after.display_name:
			embed = discord.Embed(
				color=0xadd8e6,
				title=str("Display name change"),
			)
			
			embed.add_field(name="Old nick", value=before.display_name, inline=False)
			embed.add_field(name="New nick", value=after.display_name, inline=False)


		elif before.roles != after.roles:
			if len(before.roles) < len(after.roles):
				addedRole = next(role for role in after.roles if role not in before.roles)
				description = f"Role {addedRole.name} added"
			else:
				removedRole = next(role for role in before.roles if role not in after.roles)
				description = f"Role {removedRole.name} removed"
				
			embed = discord.Embed(
				color=0xadd8e6,
				title = "Role change",
				description=str(description)
			)

		elif before.avatar != after.avatar:
			embed = discord.Embed(
				color=0xadd8e6,
				title="Avatar change",
			)
			
			embed.add_field(name="Old avatar", value=before.avatar, inline=False)
			
		else:
			return
		
		embed.add_field(name='DC Name:', value=after.mention, inline=False)
		embed.set_author(name=str(after.nick), icon_url=after.avatar)

		await logChannel.send(embed=embed)

	@commands.Cog.listener()
	async def on_voice_state_update(self, member, before, after):
		c = self.db.cursor()
		c.execute("SELECT voice_log_channel FROM guild_settings WHERE guild_id = ?", [member.guild.id])
		logChannel = self.client.get_channel(c.fetchone()[0])
		c.close()

		if after.channel is None:
			action = "left voice chat"
			
		elif before.channel != after.channel:
			action = f"joined voice chat channel {after.channel}"
			
		elif before.mute != after.mute:
			action = "muted" if after.mute else "unmuted"
			
		elif before.deaf != after.deaf:
			action = "deafed" if after.deaf else "undeafed"
			
		elif before.self_mute != after.self_mute:
			action = "self muted" if after.self_mute else "self unmuted"
			
		elif before.self_deaf != after.self_deaf:
			action = "self deafed" if after.self_deaf else "self undeafed"
			
		elif before.self_video != after.self_video:
			action = "started videostream" if after.self_video else "stopped videostream"
			
		elif before.self_stream != after.self_stream:
			action = "started live" if after.self_stream else "stopped live"
			
		else:
			action = "did something we weren't prepared for"


		embed = discord.Embed(
			title = str(member.display_name) + " " + action + ".",
			color = 0xadd8e6,
		)

		embed.set_thumbnail(url=member.avatar)

		await logChannel.send(embed=embed)

async def setup(client):
	await client.add_cog(ServerLog(client))
